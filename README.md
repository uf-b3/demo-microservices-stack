# Démo application

Ce repository sert de support pour la démonstration du fonctionnement du pipeline de CI/CD.
Le code original est disponible [ici](https://github.com/dockersamples/example-voting-app)

Nous avons donc ajouté une version légèrement modifié des fichiers ```.gitlab-ci.yml``` et ```cloudbuild.yaml``` trouvables [ici](https://gitlab.com/uf-b3/standard-ci-model)